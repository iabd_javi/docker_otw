# Utiliza una imagen base de Ubuntu
FROM ubuntu:latest

# Instala sshpass (si no está instalado)
RUN apt-get update && apt-get install -y openssh-client sshpass -y git

# Clonar repositorio y si existe lo borra
RUN rm -rf docker_otw
RUN git clone https://gitlab.com/iabd_javi/docker_otw

# Crear directorio /docker_otw
WORKDIR /docker_otw

# Concede permisos de ejecución al script
RUN chmod +x level1-7.sh

# Ejecuta el script
CMD ["./level1-7.sh"]