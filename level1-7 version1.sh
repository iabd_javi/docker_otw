#!/bin/bash

# Detalles del Servidor
num="0"
server_ip="bandit.labs.overthewire.org"
username="bandit$num"
password="bandit0"
port="2220"

# Conectar por SSH al server con usuario, contraseña, direccion ip y puerto
password=$(sshpass -p "$password" ssh "$username"@"$server_ip" -p $port 'cat readme')

echo "La clave de $username es: $password"

((num++))

username="bandit$num"

password=$(sshpass -p "$password" ssh "$username"@"$server_ip" -p $port 'cat ./-')

echo "La clave de $username es: $password"

((num++))

username="bandit$num"

password=$(sshpass -p "$password" ssh "$username"@"$server_ip" -p $port 'cat spaces\ in\ this\ filename')

echo "La clave de $username es: $password"

((num++))

username="bandit$num"

password=$(sshpass -p "$password" ssh "$username"@"$server_ip" -p $port 'cd inhere
  cat .hidden')

echo "La clave de $username es: $password"

((num++))

username="bandit$num"

password=$(sshpass -p "$password" ssh "$username"@"$server_ip" -p $port 'cd inhere
cat ./-file07')

echo "La clave de $username es: $password"

((num++))

username="bandit$num"

password=$(sshpass -p "$password" ssh "$username"@"$server_ip" -p $port 'cd inhere
  cd /home/bandit5/inhere/maybehere07/
  cat .file2')

echo "La clave de $username es: $password"

((num++))

username="bandit$num"

password=$(sshpass -p "$password" ssh "$username"@"$server_ip" -p $port 'cd /var/lib/dpkg/info/
cat bandit7.password')

echo "La clave de $username es: $password"